package controller;

import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import serviceImpl.CreateChapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class MainController {
	/*
	 * URL = https://lnmtl.com/chapter/godly-hunter-chapter-1 coreUrl =
	 * https://lnmtl.com/chapter/godly-hunter-chapter- + ChapterNum
	 */
	private static final String coreUrl = "https://lnmtl.com/chapter/charm-of-soul-pets-chapter-";

	public static void main(String[] args) throws IOException, DocumentException {
		System.setProperty("http.agent", "Chrome");
		int chpNum = 1;
		int maxChapter = 1808;
		CreateChapter createChp = new CreateChapter();
		List<Integer> fileNums = new ArrayList<Integer>();
		boolean ChpNamePresent = true; 
		String bookName = "CharmOfSoulPets";
		String coreLocToDownloadChps = "D:" + File.separator + "Books" + File.separator + bookName;
		InputStream is = null;
		byte[] dataBytes = null;
		String testString = null;
		Document doc = null;
		StringBuffer strBuffer = null;
		Elements contentElements = null;
		String fileName = null;
		File file = null;
		FileOutputStream fos = null;
		File finalFile = null;
		String chapName = "";
		Map<Integer,String> chapterName = new LinkedHashMap<Integer,String>();
		try {
			for (int i = chpNum; i < maxChapter; i++) {

				URL url = new URL(coreUrl + i);
				URLConnection con = url.openConnection();
				try {
					is = con.getInputStream();

					dataBytes = IOUtils.toByteArray(is);
					testString = new String(dataBytes, "UTF-8");
					testString = testString.replaceAll("<w", "<h4");
					testString = testString.replaceAll("</w", "</h4");

					doc = Jsoup.parse(testString);
					strBuffer = new StringBuffer();
					contentElements = doc.select("sentence.translated");
					
					chapName = doc.select("h3.dashhead-title").text();
					for (Element content : contentElements) {
						strBuffer.append(content.text());
						strBuffer.append(System.lineSeparator());
					}
					fileName = i + "";
					fileNums.add(i);
					chapterName.put(i,chapName);
					file = new File(coreLocToDownloadChps + File.separator + fileName + ".txt");
					fos = new FileOutputStream(file);
					fos.write(strBuffer.toString().getBytes());
					fos.close();
					createChp.createChapterPdf(strBuffer.toString(), file,chapName,ChpNamePresent);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			Collections.sort(fileNums);
			System.out.println(fileNums);
			finalFile = new File("D:\\Books\\CharmOfSoulPets.pdf");
			// createChp.mergePdf(finalFile, true, fileNums);
			createChp.mergePdf(finalFile,fileNums,chapterName);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
