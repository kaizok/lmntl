package serviceImpl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfCopy.PageStamp;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;

public class CreateChapter {

	public void createChapterPdf(String text, File file, String chapName, boolean ChpNamePresent)
			throws DocumentException, IOException {
		Document document = new Document();

		String outputFile = file.getAbsolutePath().replace(".txt", ".pdf");
		String fileName = file.getName().replace(".txt", "");
		File output = new File(outputFile);
		PdfWriter.getInstance(document, new FileOutputStream(output));
		document.open();
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		Paragraph p;
		Font normal = new Font(FontFamily.HELVETICA, 12);
		Font bold = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		Font chapterNum = new Font(FontFamily.TIMES_ROMAN, 18, Font.BOLD);
		boolean title = false;
		if (ChpNamePresent) {
			p = new Paragraph(chapName, bold);
			p.setAlignment(Element.ALIGN_CENTER);
		} else {
			p = new Paragraph(fileName, bold);
			p.setAlignment(Element.ALIGN_CENTER);
		}
		document.add(p);

		while ((line = br.readLine()) != null) {
			p = new Paragraph(line, title ? bold : normal);
			p.setAlignment(Element.ALIGN_JUSTIFIED);
			title = line.isEmpty();
			document.add(p);
		}
		document.addHeader("Chapter Num", fileName);
		document.close();
		br.close();
		file.delete();
	}

	public void mergePdf(File finalFile, List<Integer> fileNums, Map<Integer, String> chapterName)
			throws IOException, DocumentException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Map<Integer, String> toc = new TreeMap<Integer, String>();
		Document document = new Document();
		PdfCopy copy = new PdfCopy(document, baos);
		PageStamp stamp;
		document.open();
		int n;
		int pageNo = 0;
		PdfImportedPage page;
		Chunk chunk;
		File file = new File("D:\\Books\\CharmOfSoulPets");
		Map<String, PdfReader> filesToMerge = new LinkedHashMap<String, PdfReader>();

		if (file.isDirectory()) {
			for (int i = 0; i < fileNums.size(); i++) {
				filesToMerge.put(fileNums.get(i).toString(),
						new PdfReader(file + File.separator + fileNums.get(i) + ".pdf"));
			}

		}
		Font bold = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		for (Map.Entry<String, PdfReader> entry : filesToMerge.entrySet()) {
			n = entry.getValue().getNumberOfPages();
			toc.put(pageNo + 1, entry.getKey());
			for (int i = 0; i < n;) {
				pageNo++;
				page = copy.getImportedPage(entry.getValue(), ++i);
				stamp = copy.createPageStamp(page);
				chunk = new Chunk(String.format("Page %d", pageNo));
				if (i == 1)
					chunk.setLocalDestination("p" + pageNo);
				ColumnText.showTextAligned(stamp.getUnderContent(), Element.ALIGN_RIGHT, new Phrase(chunk), 550, 810,
						0);

				stamp.alterContents();
				copy.addPage(page);
			}
		}
		String SRC3 = "./resources/toc.pdf";
		PdfReader reader = new PdfReader(SRC3);
		page = copy.getImportedPage(reader, 1);
		stamp = copy.createPageStamp(page);
		Paragraph p;
		PdfAction action;
		PdfAnnotation link;
		float y = 770;
		ColumnText ct = new ColumnText(stamp.getOverContent());
		ct.setSimpleColumn(36, 36, 300, y);
		for (Map.Entry<Integer, String> entry : toc.entrySet()) {

			// p = new Paragraph("Chapter " + entry.getValue(), bold);
			p = new Paragraph("Chapter ", bold);
			p.add(chapterName.get(Integer.parseInt(entry.getValue())));

			// p.add(new Chunk(new DottedLineSeparator()));
			// p.add(String.valueOf(entry.getKey()));
			ct.addElement(p);
			ct.go();
			action = PdfAction.gotoLocalPage("p" + entry.getKey(), false);
			link = new PdfAnnotation(copy, 36, ct.getYLine(), 300, y, action);

			stamp.addAnnotation(link);
			y = ct.getYLine();
		}
		ct.go();
		stamp.alterContents();
		copy.addPage(page);
		document.close();
		for (PdfReader r : filesToMerge.values()) {
			r.close();
		}
		reader.close();

		reader = new PdfReader(baos.toByteArray());
		n = reader.getNumberOfPages();
		reader.selectPages(String.format("%d, 1-%d", n, n - 1));
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(finalFile));
		stamper.close();
	}

	public static void main(String[] args) throws IOException, DocumentException {
		// step 1
		Document document = new Document();
		String SRC3 = "./resources/toc.pdf";
		// step 2
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(SRC3));
		// step 3
		document.open();
		// step 4
		document.add(new Paragraph("This page will NOT be followed by a blank page!"));
		document.newPage();
		// we don't add anything to this page: newPage() will be ignored
		document.newPage();
		document.add(new Paragraph("This page will be followed by a blank page!"));
		document.newPage();
		writer.setPageEmpty(false);
		document.newPage();
		document.add(new Paragraph("The previous page was a blank page!"));
		// step 5
		document.close();

	}
}
